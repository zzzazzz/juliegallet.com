<?php
/**
 * Plugin Microedtion
 * 2019 Fa_b
 * Licence GNU/GPL
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


function microedition_ieconfig_metas($table){
    $table['microedition']['titre'] = _T('microedition:cfg_titre_page_configurer_microedition');
    $table['microedition']['icone'] = 'prive/themes/spip/images/microedition-32.png';
    $table['microedition']['metas_serialize'] = 'microedition';
    return $table;
}
