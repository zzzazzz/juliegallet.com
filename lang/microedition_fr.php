<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
$GLOBALS[$GLOBALS['idx_lang']] = array(
  'configurer_microedition' => ' Micro-Édition',
  'cfg_titre_parametrages' => 'Page de configuration du plugin',
  'cfg_label_rubrique' 		=> 'Quelle rubrique en page d’accueil',
	'cfg_rubrique_explication' 	=> 'Mettre ici l’identifiant de la rubrique',
  'cfg_label_rezo'  => 'Réseau sociaux',
  'cfg_label_rezo_instagram' => 'Instagram',
  'cfg_rezo_explication' => 'Saisir l’adresse complète du profil.',
  'cfg_label_rezo_twitter'  => 'Twitter',
  'cfg_label_rezo_facebook'  => 'Facebook',
   );
?>
