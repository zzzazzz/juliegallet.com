# Micro édition

Jeu de squelettes SPIP simple pour le site [juliegallet.com](http://www.juliegallet.com)

Mots clés : portfolio, micro-edition, petit site, rédactionnel limité

À l'installation, le plugin modifie deux infos de config :
- change la valeur html4 par défaut de SPIP en html5
- utilise la *skin* «white-simple» du plugin-dist mediabox

## Les plugins

### `<necessite>`

**Z-Core** (zcore)
- organisation structurelle du jeu de squelettes.

**Pages Uniques** (pages)
- pour les pages « about » et « contact »

----

### `<utilise>`

**Compositions** (compositions)

- rubriques
 - Carrousel
 - Portfolio 2 colonnes
 - Portfolio 3 colonnes (redirection vers article portfolio 3 col)

- articles
 - portfolio 2 colonnes
 - portfolio 3 colonnes
 - page unique
 - page contact

**embed** (oembed)
- pour la facilité d'ajout des vidéos

**Image responsive** (image_responsive)
- ne pas oublier d'ajouter le htaccess présent à la racine due ce plugin

**Rang** (rang)
- Pour trier les articles et les rubriques

**Court Circuit**
- Pour aller directement sur l'article quand une rubrique ne contient qu'un article

**Pages uniques** (pages)
- pour les pages contact et about


**Owl Carousel** (owlcarousel)
- Utilisé pour les rubriques avec des images sans articles
- Utilisé pour un article isolé dans sa rubrique (court-circuit)

----

### Conseillé

**No Spam** (nospam)

----

## TODO
* [x]  Ajouter une nouvelle composition type portfolio
* [x]  Meilleure gestion responsive
* [x]  #CONFIG : mettre du texte sur les labels
* [ ]  Éventuellement passer par le plugin knacss pour la css principale
