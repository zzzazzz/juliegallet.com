<?php

function microedition_upgrade($nom_meta_base_version, $version_cible) {

    $maj = array();
    $maj['install'] = array(
        array('microedition_ecrire_config'),
    );
    include_spip('base/upgrade');
    maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * configure mediabox et SPIP lors de l'installation du squelette
**/
function microedition_ecrire_config() {

	ecrire_config('mediabox/skin','white-simple');
	ecrire_config('version_html_max','html5');

}
